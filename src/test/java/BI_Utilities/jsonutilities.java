package BI_Utilities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.json.*;
import org.openqa.selenium.json.JsonException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class jsonutilities {
	
	
public static ArrayList<HashMap<String,String>> RetrievejsonDetails(String downloadpath, String jobid, String fieldstobeExtracted, String tcid) throws Exception {
		

	ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>() ;
		
		try{
			String fileName=null;
			//Retrievefilename
			File[] listFiles = new File(downloadpath).listFiles();

			for (int i = 0; i < listFiles.length; i++) {

			    if (listFiles[i].isFile()) {
			        fileName = listFiles[i].getName();
			        
			        if (fileName.startsWith(jobid)
			                && fileName.endsWith(".json")) {
			            
			            System.out.println("fileName:"+fileName);
			            break;
			        }
			        	
			    }
			}
			if(fileName==null){
				
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
			
			String[] fieldsToBeExtractedArray=fieldstobeExtracted.trim().split(",");
//			FileReader fReader = new FileReader(downloadpath+fileName);
			Scanner sReader = new Scanner(new File(downloadpath+fileName));
			
			System.out.println(downloadpath);
			System.out.println(fileName);
			
			String jsonresponse = sReader.useDelimiter("\\Z").next();
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        JsonArray jarray = jsonobject.getAsJsonArray("orderProposals");
	        System.out.println(jarray.size());

	        for(int i=0; i<jarray.size(); i++) {
	        	HashMap<String,String> subres=new HashMap<String,String>();
	        	jsonelement= jarray.get(i).getAsJsonObject();
	        	jsonobject = jsonelement.getAsJsonObject();
	        	System.out.println(jsonobject.toString());
	        	System.out.println(jsonobject.size());
	        	JsonElement tempjsonobject = jsonobject.get("proposedOrder");
	        	System.out.println(tempjsonobject);
	        	jsonobject = tempjsonobject.getAsJsonObject();

		        	for(int fields=0;fields<fieldsToBeExtractedArray.length;fields++){
		        		try{
		        			String lookupsJsonKey = jsonobject.get(fieldsToBeExtractedArray[fields]).toString().replaceAll("^\"|\"$", "");
		        			System.out.println(fieldsToBeExtractedArray[fields]+" = "+lookupsJsonKey);
			        		subres.put(fieldsToBeExtractedArray[fields].toString(), lookupsJsonKey.toString());
		        		}catch(Exception e){
		        			System.out.println(fieldsToBeExtractedArray[fields].toString()+" is not present in the json file");
		        		}
		        		
		        		
		        	}
		        	

		        	res.add(subres);
	        }
	        sReader.close();
	    	
		} catch (Exception e) {
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
			
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				
		}finally{
			return res;
		}
		
	}

public static ArrayList<HashMap<String,String>> RetrievefieldDetailscsv(String downloadpath, String filename, String fieldstobeExtracted, String jArrayVal, String tcid) throws Exception {
	

	ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>() ;
		
	try{
			
			if(filename==null){
				
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
			
			File file = new File(downloadpath+filename+".csv");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			if(jArrayVal==""){
				jArrayVal=headers[0];
				line="\"{\""+jArrayVal+"\":\""+line.replace("\"[", "[").replace("]\"", "]")+"}";
			}
			
			while(line != null)
			{
				/*Scanner sReader=line;
				String jsonresponse = sReader.useDelimiter("\\Z").next();*/
				String jsonresponse =line.replace("\"\"", "\"").replace("\"{", "{").replace("}\"", "}").replace("\"[", "[").replace("]\"", "]");
		        System.out.println(jsonresponse);
		        utilityFileWriteOP.writeToLog(tcid, "The JSON format  ", "Displayed as "+jsonresponse);
		        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        System.out.println(jsonobject.toString());
		        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
//		        JsonArray jarray = jsonobject.getAsJsonArray("requestEvent");
		        JsonArray jarray = jsonobject.getAsJsonArray(jArrayVal);
		        System.out.println(jarray.size());

		        for(int i=0; i<jarray.size(); i++) {
		        	HashMap<String,String> subres=new HashMap<String,String>();
		        	jsonelement= jarray.get(i).getAsJsonObject();
		        	jsonobject = jsonelement.getAsJsonObject();
		        	System.out.println(jsonobject.toString());
		        	System.out.println(jsonobject.size());
//		        	JsonElement tempjsonobject = jsonobject.get("proposedOrder");
		        	System.out.println(jsonobject);
		        	String[] fieldsToBeExtractedArray=null;
		        	try{
		        		fieldsToBeExtractedArray = fieldstobeExtracted.split(",");
		        	}catch(Exception e){
		        		fieldsToBeExtractedArray[0]=fieldstobeExtracted;
		        	}
		        	
		        	jsonobject = jsonobject.getAsJsonObject();

			        	for(int fields=0;fields<fieldsToBeExtractedArray.length;fields++){

			        		String lookupsJsonKey = jsonobject.get(fieldsToBeExtractedArray[fields]).toString().replaceAll("^\"|\"$", "");
			        		System.out.println(fieldsToBeExtractedArray[fields]);
			        		subres.put(fieldsToBeExtractedArray[fields].toString(), lookupsJsonKey.toString());
			        	}
			        	

			        	res.add(subres);
		        }
		        line = br.readLine();
			}
		
			
	        br.close();
	    	
		} catch (Exception e) {
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
			
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				
		}finally{
			return res;
		}
		
	}

public static ArrayList<HashMap<String,String>> RetrievefieldDetailjonSinglearyaysCSV(String downloadpath, String filename, String fieldstobeExtracted, String Resultpath, String tcid, XWPFRun xwpfrun) throws Exception {
	

	ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>() ;
	HashMap<String,String> subres = new HashMap<String,String>();
		try{
			
			//Retrievefilename
			if(filename==null){
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
			File file=null;
			FileReader fr;
			try{
				file = new File(downloadpath+filename+".csv");
				fr = new FileReader(file);
			}catch(Exception e){
				file = new File(downloadpath+filename);
				fr = new FileReader(file);
			}
			
			
			BufferedReader br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			while(line != null)
			{
				
				String jsonresponse =line.replace("\"\"", "\"").replace("\"{", "{").replace("}\"", "}");
		        System.out.println(jsonresponse);
		        utilityFileWriteOP.writeToLog(tcid, "The JSON format  ", "Displayed as "+jsonresponse, Resultpath);
		        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
		        JsonObject jsonobject = jsonelement.getAsJsonObject();
		        System.out.println(jsonobject.toString());
		        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString(), Resultpath);
		        System.out.println(jsonobject);
		        String[] fieldsToBeExtractedArray=null;
		        try{
		        	fieldsToBeExtractedArray = fieldstobeExtracted.split(",");
		        }catch(Exception e){
		        	fieldsToBeExtractedArray[0]=fieldstobeExtracted;
		        }
		        jsonobject = jsonobject.getAsJsonObject();
		        for(int fields=0;fields<fieldsToBeExtractedArray.length;fields++){
		        	String lookupsJsonKey = jsonobject.get(fieldsToBeExtractedArray[fields]).toString().replaceAll("^\"|\"$", "");
		        	System.out.println(fieldsToBeExtractedArray[fields]);
		        	subres.put(fieldsToBeExtractedArray[fields].toString(), lookupsJsonKey.toString());
		        }
			        	

			        	res.add(subres);
		        
		        line = br.readLine();
			}
		
			
	        br.close();
	    	
		} catch (Exception e) {
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e, Resultpath);
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				
		}finally{
			return res;
		}
		
	}

public static boolean comparebtw2jsonfiles(
		HashMap<String, String> pOCreateOrderjsonExtract,
		HashMap<String, String> pOReqResjsonExtract,
		HashMap<String, String> fieldmapping, ExtentTest logger,
		String Resultpath, String tcid, XWPFRun xwpfrun) {
	boolean res=false;
	Iterator<?> it = fieldmapping.entrySet().iterator();
    String srcvalue=null, trgtvalue=null;
    while (it.hasNext()) {
        Map.Entry pair = (Map.Entry)it.next();
        System.out.println(pair.getKey() + " = " + pair.getValue());
        srcvalue=pOCreateOrderjsonExtract.get(pair.getKey());
        trgtvalue=pOReqResjsonExtract.get(pair.getValue());
        if(trgtvalue.trim().contains(srcvalue.trim())){
        	res=true;
        	logger.log(LogStatus.PASS,"Validaion of "+pair.getKey()+":successful in json files. Value displayed as "+srcvalue);
        	
        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Successful in json files. Value displayed as "+srcvalue,Resultpath);
        }else{
        	res=false;
        	logger.log(LogStatus.FAIL,"Validaion of "+pair.getKey()+":Unsuccessful in jsonfiles. Expected value: "+srcvalue+". Actual Value: "+trgtvalue);
        	
        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Not Successful in json files. Expected value: "+srcvalue+". Actual Value: "+trgtvalue,System.getProperty("user.dir")+Resultpath);
        }
        	
        it.remove(); // avoids a ConcurrentModificationException
    }
	return res;
}

public static ArrayList<HashMap<String,String>> RetrievefieldDetailjonSinglearyaysJSON(String downloadpath, String filename, String fieldstobeExtracted, String singlearrfield,String Resultpath, String tcid, XWPFRun xwpfrun) throws Exception {
	

	ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>() ;
	
	try{
		String fileName=null;
		//Retrievefilename
		File[] listFiles = new File(downloadpath).listFiles();

		for (int i = 0; i < listFiles.length; i++) {

		    if (listFiles[i].isFile()) {
		        fileName = listFiles[i].getName();
		        
		        if (fileName.startsWith(filename)
		                && fileName.endsWith(".json")) {
		            
		            System.out.println("fileName:"+fileName);
		            break;
		        }else{
		        	fileName=null;
		        }
		        	
		    }
		}
		if(fileName==null){
			
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
		}
			//Retrievefilename
			if(filename==null){
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
			
			
			String[] fieldsToBeExtractedArray=fieldstobeExtracted.trim().split(",");
//			FileReader fReader = new FileReader(downloadpath+fileName);
			Scanner sReader = new Scanner(new File(downloadpath+fileName));
			String jsonresponse = sReader.useDelimiter("\\Z").next();
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        JsonArray jarray = jsonobject.getAsJsonArray(singlearrfield);
	        System.out.println(jarray.size());

	        for(int i=0; i<jarray.size(); i++) {
	        	HashMap<String,String> subres=new HashMap<String,String>();
	        	jsonelement= jarray.get(i).getAsJsonObject();
	        	jsonobject = jsonelement.getAsJsonObject();
	        	System.out.println(jsonobject.toString());
	        	System.out.println(jsonobject.size());
//	        	JsonElement tempjsonobject = jsonobject.get("proposedOrder");
//	        	System.out.println(tempjsonobject);
//	        	jsonobject = tempjsonobject.getAsJsonObject();

		        	for(int fields=0;fields<fieldsToBeExtractedArray.length;fields++){
		        		try{
		        			String lookupsJsonKey = jsonobject.get(fieldsToBeExtractedArray[fields]).toString().replaceAll("^\"|\"$", "");
		        			System.out.println(fieldsToBeExtractedArray[fields]+" = "+lookupsJsonKey);
			        		subres.put(fieldsToBeExtractedArray[fields].toString(), lookupsJsonKey.toString());
		        		}catch(Exception e){
		        			System.out.println(fieldsToBeExtractedArray[fields].toString()+" is not present in the json file");
		        		}
		        		
		        		
		        	}
		        	

		        	res.add(subres);
	        }
	        sReader.close();
	    	
	    	
		} catch (Exception e) {
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e, Resultpath);
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				
		}finally{
			return res;
		}
		
	}

public static String RetrieveTextjonSinglearyaysJSON(
		String downloadpath, String filename,
		String fieldstovalidate_poreqres_injson,
		String Resultpath, String tcid, XWPFRun xwpfrun) {
	// TODO Auto-generated method stub
	String res=null;
	
	try{
		
		//Retrievefilename
		if(filename==null){
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
		}
		File file=null;
		FileReader fr;
		try{
			file = new File(downloadpath+filename+".csv");
			fr = new FileReader(file);
		}catch(Exception e){
			file = new File(downloadpath+filename);
			fr = new FileReader(file);
		}
		
		
		BufferedReader br = new BufferedReader(fr);
		String line, header;
//		String[] headers=br.readLine().split(","); // consume first line and ignore
		line = br.readLine();
		while(line != null)
		{
			if(line.contains(fieldstovalidate_poreqres_injson)){
				int index=line.indexOf(fieldstovalidate_poreqres_injson);
//				res= line.substring(line.indexOf(fieldstovalidate_poreqres_injson) + 16, line.indexOf(fieldstovalidate_poreqres_injson) + 23);
				res= line.substring(index+18, index+40).trim();
				break;
			}
			
			line = br.readLine();
		}
	
	
	
	}catch(Exception e){

		e.printStackTrace();
		utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e, Resultpath);
		throw new MyException("Test Stopped Because of Failure. Please check Execution log");
	}finally{
		return res;
	}
	
}

public static ArrayList<HashMap<String,String>> RetrievefieldDetailjonFieldsJSON(String downloadpath, String filename, String parentField,String fieldstobeExtracted, String singlearrfield,String Resultpath, String tcid, XWPFRun xwpfrun) throws Exception {
	

	ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>() ;
	
	try{
		String fileName=null;
		//Retrievefilename
		File[] listFiles = new File(downloadpath).listFiles();

		for (int i = 0; i < listFiles.length; i++) {

		    if (listFiles[i].isFile()) {
		        fileName = listFiles[i].getName();
		        
		        if (fileName.startsWith(filename)
		                && fileName.endsWith(".json")) {
		            
		            System.out.println("fileName:"+fileName);
		            break;
		        }else{
		        	fileName=null;
		        }
		        	
		    }
		}
		if(fileName==null){
			
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
		}
			//Retrievefilename
			if(filename==null){
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
			
			
			String[] fieldsToBeExtractedArray=fieldstobeExtracted.trim().split(",");
//			FileReader fReader = new FileReader(downloadpath+fileName);
			Scanner sReader = new Scanner(new File(downloadpath+fileName));
			String jsonresponse = sReader.useDelimiter("\\Z").next();
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        JsonObject subjsonobject = jsonobject.getAsJsonObject(parentField).getAsJsonObject(singlearrfield);
	        System.out.println(subjsonobject.toString());
	        
	       
	        	HashMap<String,String> subres=new HashMap<String,String>();
	        	for(int fields=0;fields<fieldsToBeExtractedArray.length;fields++){
	        		try{
	        				String lookupsJsonKey = subjsonobject.get(fieldsToBeExtractedArray[fields]).toString().replaceAll("^\"|\"$", "");
	        				System.out.println(fieldsToBeExtractedArray[fields]+" = "+lookupsJsonKey);
	        				subres.put(fieldsToBeExtractedArray[fields].toString(), lookupsJsonKey.toString());
		        		}catch(Exception e){
		        			System.out.println(fieldsToBeExtractedArray[fields].toString()+" is not present in the json file");
		        		}
		        		
		        		
		        	}
		        	

		        	res.add(subres);
	        
	        sReader.close();
	    	
	    	
		} catch (Exception e) {
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e, Resultpath);
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				
		}finally{
			return res;
		}
		
	}
public static Map<String, Object> toMap(JsonObject object) throws JsonException {
    Map<String, Object> map = new HashMap<String, Object>();

    Set<Entry<String, JsonElement>> entrySet = object.entrySet();
    for(Map.Entry<String,JsonElement> entry : entrySet){
        String key = entry.getKey();
        Object value = object.get(key);

        if(value instanceof JsonArray) {
            value = toList((JsonArray) value);
        }

        else if(value instanceof JsonObject) {
            value = toMap((JsonObject) value);
        }
        map.put(key, value);
    }
    return map;
}

public static List<Object> toList(JsonArray array) throws JsonException {
    List<Object> list = new ArrayList<Object>();
    for(int i = 0; i < array.size(); i++) {
        Object value = array.get(i);
        if(value instanceof JsonArray) {
            value = toList((JsonArray) value);
        }

        else if(value instanceof JsonObject) {
            value = toMap((JsonObject) value);
        }
        list.add(value);
    }
    return list;
}
public static Map<String, Object> jsonToMap(JsonObject json) throws JsonException {
    Map<String, Object> retMap = new HashMap<String, Object>();

    if(json != null) {
        retMap = toMap(json);
    }
    return retMap;
    
}
@SuppressWarnings("unchecked")
public static ArrayList<HashMap<String, String>> RetrievejsonDetailsAmmendment(String downloadpath, String jobid, String fieldstobeExtracted, String tcid) throws Exception {
	

	ArrayList<HashMap<String,String>> res = new ArrayList<HashMap<String,String>>() ;
		
		try{
			String fileName=null;
			//Retrievefilename
			File[] listFiles = new File(downloadpath).listFiles();

			for (int i = 0; i < listFiles.length; i++) {

			    if (listFiles[i].isFile()) {
			        fileName = listFiles[i].getName();
			        
			        if (fileName.startsWith(jobid)
			                && fileName.endsWith(".json")) {
			            
			            System.out.println("fileName:"+fileName);
			            break;
			        }
			        	
			    }
			}
			if(fileName==null){
				
				throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			}
			
			String[] fieldsToBeExtractedArray=fieldstobeExtracted.trim().split(",");
//			FileReader fReader = new FileReader(downloadpath+fileName);
			Scanner sReader = new Scanner(new File(downloadpath+fileName));
			String jsonresponse = sReader.useDelimiter("\\Z").next();
	        System.out.println(jsonresponse);
	        utilityFileWriteOP.writeToLog(tcid, "The JSON response  ", "Displayed as "+jsonresponse);
	        JsonElement jsonelement = new JsonParser().parse(jsonresponse);
	        JsonObject jsonobject = jsonelement.getAsJsonObject();
	        System.out.println(jsonobject.toString());
	        utilityFileWriteOP.writeToLog(tcid, "The JSON Object body ", "Displayed as "+jsonobject.toString());
	        JsonArray jarray = jsonobject.getAsJsonArray("updatedProposals");
	        System.out.println(jarray.size());

	        for(int i=0; i<jarray.size(); i++) {
	        	Map<String, Object> subres=new HashMap<String,Object>();
	        	jsonelement= jarray.get(i).getAsJsonObject();
	        	jsonobject = jsonelement.getAsJsonObject();
	        	subres=jsonToMap(jsonobject);
		        //Convert Convert Map<String,Object> to Map<String,String>	
	        	HashMap<String,Object> newMaptemp =new HashMap<String,Object>();
	        	HashMap<String,String> newMap =new HashMap<String,String>();
	        	
	        	for (Map.Entry<String, Object> entry : subres.entrySet()) {
	        		newMaptemp.putAll((HashMap<String, Object>) entry.getValue());
	        		for (Map.Entry<String, Object> entrytmp : newMaptemp.entrySet()) {
	        			
	        	            newMap.put(entrytmp.getKey(),entrytmp.getValue().toString().replaceAll("^\"|\"$", ""));
	        	        
	        		}
	        	       
	        	 }
	        	
	        	//**********************************************
		        	res.add(newMap);
	        }
	        sReader.close();
	    	
		} catch (Exception e) {
			
			e.printStackTrace();
			utilityFileWriteOP.writeToLog(tcid, "Error occurred during GET call ", "Due to :"+e);
			
			throw new MyException("Test Stopped Because of Failure. Please check Execution log");
				
		}finally{
			return res;
		}
		
	}

}
