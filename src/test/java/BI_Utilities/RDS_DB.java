package BI_Utilities;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import BI_Utilities.utilityFileWriteOP;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import jxl.read.biff.BiffException;

import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.jcraft.jsch.*;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class RDS_DB {
	
	
	
	
	public static boolean isInteger(String str) {
	    if (str == null) {
	        return false;
	    }
	    int length = str.length();
	    if (length == 0) {
	        return false;
	    }
	    int i = 0;
	    if (str.charAt(0) == '-') {
	        if (length == 1) {
	            return false;
	        }
	       
	        i = 0;
	    }
	    for (; i < length; i++) {
	        char c = str.charAt(i);
	        if (c < '0' || c > '9') {
	            return false;
	        }
	    }
	    return true;
	}
	
	
	public static boolean isNumeric(String str)
	{
	  return str.matches("-?\\d+(\\.\\d+)?");  //match a number with optional '-' and decimal.
	}

	
	
	
	public static boolean isStringNumeric( String str )
	{
		boolean isNumeric = str.chars().allMatch( Character::isDigit );
		
		return isNumeric;
	}
	
	
	
	@SuppressWarnings("finally")
	public static String getAdjustMentValueRDS(String item_no,String location ,String tcid) throws JSchException{
	
		 Channel channel=null;
		 Session session=null;
	     String adj_val_rds="0";    
	     try{      
	    	 JSch jsch = new JSch();
	    	 java.util.Properties config = new java.util.Properties(); 
	    	 String command = "psql --host=orderservice-sit-00.ca107skwgezh.eu-west-1.rds.amazonaws.com NonProdSitOrderService --user master;";
	    	 session = jsch.getSession("mwadmin","10.244.39.83",22);
	    	 config.put("StrictHostKeyChecking", "no");
	    	 session.setConfig(config);
	    	 session.setPassword("2Bchanged");
	    	 session.connect();
	    	 channel = session.openChannel("exec");
	    	 ((ChannelExec)channel).setCommand(command);
	    	 channel.setInputStream(null);
	    	 ((ChannelExec)channel).setErrStream(System.err);
	    	 InputStream in = channel.getInputStream();
	    	 ((ChannelExec)channel).setPty(true);
	    	 OutputStream out=channel.getOutputStream();
	    	 channel.connect();
	    	 Thread.sleep(1000);
	         out.write(("Uighie8aaghiechi"+"\n").getBytes());
	         out.flush();
	         Thread.sleep(1000);
	         out.write(("select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());
	         utilityFileWriteOP.writeToLog(tcid, "select value,min from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;", " ");
	         out.flush();
	         Thread.sleep(1000);
	         out.write(("\\q"+"\n").getBytes());
	         out.flush();
	         Thread.sleep(5000);
	         byte[] tmp = new byte[2048];
	         int i=0;  
	         while (in.available() > 0) {
	        	 i = in.read(tmp, 0, 1024);
        		 if (i < 0) {
        			 break;
        		 }
	         }
	         String QResult=new String(tmp, 0, i); 
	         String [] res=   QResult.split(" ");
	         utilityFileWriteOP.writeToLog( QResult);
	         for(int j=0;j<res.length;j++){
	        	 if(RDS_DB.isInteger(res[j])){
	        		 adj_val_rds=res[j].trim();
	    		 }  
	         }
	         channel.disconnect();
	         session.disconnect();
	         Thread.sleep(5000);
	         out.close();
	         in.close();
	     }catch(Exception e){
	    	 System.out.println(e);
	     }finally{        
	    	 if (channel != null) {
	    		 session = channel.getSession();
	    		 channel.disconnect();
	    		 session.disconnect();
	    	 }
	    	 return adj_val_rds;
	     }
	}
	public static String[] retrieveDatafromRDS(String SQL, String tcid) throws JSchException{
		
		 Channel channel=null;
		 Session session=null;
	     String [] res = null;
	     try{      
	    	 JSch jsch = new JSch();
	    	 java.util.Properties config = new java.util.Properties(); 
	    	 String command = "psql --host=orderservice-sit-00.ca107skwgezh.eu-west-1.rds.amazonaws.com NonProdSitOrderService --user master;";
	    	 session = jsch.getSession("mwadmin","10.244.39.83",22);
	    	 config.put("StrictHostKeyChecking", "no");
	    	 session.setConfig(config);
	    	 session.setPassword("2Bchanged");
	    	 session.connect();
	    	 channel = session.openChannel("exec");
	    	 ((ChannelExec)channel).setCommand(command);
	    	 channel.setInputStream(null);
	    	 ((ChannelExec)channel).setErrStream(System.err);
	    	 InputStream in = channel.getInputStream();
	    	 ((ChannelExec)channel).setPty(true);
	    	 OutputStream out=channel.getOutputStream();
	    	 channel.connect();
	    	 Thread.sleep(1000);
	         out.write(("Uighie8aaghiechi"+"\n").getBytes());
	         out.flush();
	         Thread.sleep(1000);
//	         out.write(("select orderAlternateID,supplierInfo,shippingInfo,currentReqStatus from tOrderRequests where orderAlternateID ='"+order_no+"';"+"\n").getBytes());
	         out.write((SQL+"\n").getBytes());
	         utilityFileWriteOP.writeToLog(tcid, SQL, " ");
	         out.flush();
	         Thread.sleep(1000);
	         out.write(("\\q"+"\n").getBytes());
	         out.flush();
	         Thread.sleep(5000);
	         byte[] tmp = new byte[4096];
	         int i=0;  
	         while (in.available() > 0) {
	        	 i = in.read(tmp, 0, 4096);
       		 if (i < 0) {
       			 break;
       		 }
	         }
	         String QResult=new String(tmp, 0, i); 
	         
	         
	         
	         
	         System.out.println(QResult);
	         res=   QResult.split(" ");
	         utilityFileWriteOP.writeToLog( QResult);
	         for(int j=0;j<res.length;j++){
	        	 if(RDS_DB.isInteger(res[j])){
	        		 res[j].trim();
	    		 }  
	         }
	         channel.disconnect();
	         session.disconnect();
	         Thread.sleep(5000);
	         out.close();
	         in.close();
	         return res;
	     }catch(Exception e){
	    	 System.out.println(e);
	    	 res=null;
	    	 return res;
	     }finally{        
	    	 if (channel != null) {
	    		 session = channel.getSession();
	    		 channel.disconnect();
	    		 session.disconnect();
	    	 }
	    	 
	     }
	}

  public static void main(String[] args) throws Exception {

    	 String item_no="101570467";  
  	     String location="89";
  	     String tcid="01";
  	     
    RDS_DB.getAdjustMentValueRDS(item_no, location ,tcid);
    	 
    	 
  

     }
  
  public static boolean GetSourcepathFromRDS(String RDSusername, String RDSpassword, String RDSHostName, int RDSPort,String Query ,String CSVFileName,  String ResultPath, String tcid, XWPFRun xwpfRun ) throws JSchException, BiffException, IOException{
		System.out.println("Inside RDS");
		 Channel channel=null;
		 Session session=null;
		 String connectionString=ProjectConfigurations.LoadProperties("ConnectionString");
		 String connectionPwd=ProjectConfigurations.LoadProperties("ConnectionPwd");
	     String QResult="";
	     Boolean res=false;
	        try{      
	                
	        	  JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
//	              String command = "psql --host=orderservice-sit-00.ca107skwgezh.eu-west-1.rds.amazonaws.com NonProdSitOrderService --user master;";           
	              String command = connectionString.trim();
	              session = jsch.getSession(RDSusername,RDSHostName,RDSPort);
	              
	              config.put("StrictHostKeyChecking", "no");
		          session.setConfig(config);
	      
	              session.setPassword(RDSpassword);
	          
	          	  session.connect();
	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);
	              
	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(true);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
//	              out.write(("Uighie8aaghiechi"+"\n").getBytes());
	              out.write((connectionPwd.trim()+"\n").getBytes());
			        Thread.sleep(1000);
			        out.flush();
			        Thread.sleep(2000);
	   
	      
	        out.write(("\\copy ("+Query+") to "+CSVFileName+" with CSV Header; \n").getBytes());
	        out.flush();    
	        Thread.sleep(5000);
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        String line;
	        byte[] tmp = new byte[4096];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0)
	           {
	          		i = in.read(tmp, 0, 4096);
               if (i < 0) 
               {
	                        break; 
	             }
	           }     
	         
	          	System.out.print(new String(tmp, 0, i));
	        	QResult=new String(tmp, 0, i);
	        	String QResultFinal=new String(tmp, 0, i).replace("copy ", ""); 
	        	if(QResult!=null)
	        	{ 
	        		res=true;
	        	System.out.println("QResult is "+QResult+" is this!!!");
	        	utilityFileWriteOP.writeToLog(tcid,"Query result stored in a CSV file named  ",CSVFileName ,ResultPath,xwpfRun,"");
			 
	        	utilityFileWriteOP.writeToLog(tcid, "Query Result is-", QResultFinal,ResultPath, xwpfRun, "") ;
	        	Thread.sleep(5000);
	        	}
	        	else
	        	{
                 res=false;
                 utilityFileWriteOP.writeToLog(tcid,"Query result is null  ", QResult,ResultPath,xwpfRun,"");
	        	}
				  channel.disconnect();	
				  session.disconnect();
				  Thread.sleep(5000);		  
				  out.close();		  
				  in.close();
				  
	        	}catch(Exception e)
			        {
						e.printStackTrace();
						res = false;
						utilityFileWriteOP.writeToLog(tcid, "Could not get Result from RDS due to ", e.toString(),ResultPath, xwpfRun,"") ;
						return res;
						
			        } 
			
			
			finally{

				    if (channel != null) 
				    {
				      session = channel.getSession();
				        channel.disconnect();
				        session.disconnect();
	   
				    }					
			      
			}
			        return res;
			}

	
  public static boolean MoveCSVFileFromPuttyUNIXtoLocal( String PuttyPath ,String CSV_Folder,String CSVFileName,  String ResultPath, String tcid, XWPFRun xwpfRun ) throws JSchException, BiffException, IOException{
		
		
		System.out.println("Inside RDS");
		 Channel channel=null;
		 Session session=null;

	     String QResult="";
	     Boolean res=false;
		        
				        Process p1=  Runtime.getRuntime().exec(PuttyPath+"\\pscp.exe -pw 2Bchanged mwadmin@10.244.39.83:/home/mwadmin/"+CSVFileName+" "+CSV_Folder+"");
				        System.out.println(PuttyPath+"\\pscp.exe -pw 2Bchanged mwadmin@10.244.39.83:/home/mwadmin/"+CSVFileName+" "+CSV_Folder+"");
				        try {
							p1.waitFor();
							
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							res=false;
						}
				       
				        Process p2=  Runtime.getRuntime().exec("S:\\Putty\\psftp mwadmin@10.244.39.83 -P 22 -pw 2Bchanged -b \""+CSV_Folder+"ErithBatch.bat "+CSVFileName+"\"");
				        System.out.println("S:\\Putty\\psftp mwadmin@10.244.39.83 -P 22 -pw 2Bchanged -b \""+CSV_Folder+"ErithBatch.bat "+CSVFileName+"\"");
				        try {
							p2.waitFor();
							 res=true;
							
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							res=false;
						}
						return res;

	}
  
	@SuppressWarnings("finally")
	public static String getAdjustMentValueRDS(String User,String Pass,String item_no,String location ,String tcid,String ResultPath,XWPFRun xwpfRun) throws JSchException{
	
		 Channel channel=null;
		 Session session=null;
	     
	     String adj_val_rds="";    

	        try{      
	              
	              JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              
	              	//For SIT
	              String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_sit --user tcs_testing;";
	              
	              //For UAT
	              //String command = "psql --host=generic01-nonprod-nonprod-rdsgeneric-del.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stock_v1_db_uat --user tcs_testing;";

	              
	            session = jsch.getSession(User,"10.244.39.83",22);
	              
	             config.put("StrictHostKeyChecking", "no");
	              
	              session.setConfig(config);
	              
	             
	              
	          session.setPassword(Pass);
	          
	          session.connect();
	              
	           //   System.out.println("Connection success");

	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);

	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(true);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              
	        out.write(("f55c9c05877d"+"\n").getBytes());
	        out.flush();
	        
	        Thread.sleep(1000);

	     out.write(("select value from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;"+"\n").getBytes());

	     utilityFileWriteOP.writeToLog(tcid, "select value from transaction_hist where min ='"+item_no+"' and location_id='"+location+"' order by created desc limit 1;", " ",ResultPath,xwpfRun,"");
	        out.flush();
	        
	        Thread.sleep(1000);
	        
	        out.write(("\\q"+"\n").getBytes());
	        
	        out.flush();
	        
	        Thread.sleep(5000);

	     // channel.setOutputStream(System.out);

	        
	        
	        byte[] tmp = new byte[2048];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0) {
	                	
	                	{
	                  
	                i = in.read(tmp, 0, 1024);
	                   
	                    
	                  //  System.out.println(i);
	                    
	                    if (i < 0) {
	                        break;
	 
	                    }
	                  
	           //   System.out.print(new String(tmp, 0, i));
	                    
	                }  
	                	
	                	
	               
	               }
	          
	        String QResult=new String(tmp, 0, i); 
	           
	        //String [] res1=   QResult.split("value");

	        String [] res=   QResult.split("\n");

	        
	        System.out.println(QResult);
	        
	    	utilityFileWriteOP.writeToLog(tcid, QResult,"",ResultPath,xwpfRun,"");	
	        
	        for(int j=0;j<res.length;j++)
		   	   {
	        	
		   		   if (res[j].replaceAll("\\|", "").trim().equalsIgnoreCase("(0 rows)")) 
		   		   {
		   			System.out.println("No results Found");
			    	utilityFileWriteOP.writeToLog(tcid, "No results Found","",ResultPath,xwpfRun,"");	
			    	return adj_val_rds;
		   		   }
		   	   }
	        
	        for (int j = 0; j < res.length; j++) {
	     	   if(RDS_DB.isInteger(res[j].replaceAll("\\|", "").trim())){
	     		   
	     		   	adj_val_rds = res[j].replaceAll("\\|", "").trim();
	     			System.out.println(adj_val_rds);
	     			
			    	utilityFileWriteOP.writeToLog(tcid, "Adjustment Value ",adj_val_rds,ResultPath,xwpfRun,"");	


	     	     }  
	        }
	        
	   
	// System.out.println( session.isConnected());

	  channel.disconnect();

	  session.disconnect();
	  
	  Thread.sleep(5000);
	  
	  out.close();
	  
	  in.close();
	  
	//System.out.println(adj_val_rds);
	  
	 

}
	              

	        
catch(Exception e){

System.out.println(e);

utilityFileWriteOP.writeToLog(tcid, "Error Occured While Fetching Adjustment Value from transaction_hist ","",ResultPath,xwpfRun,"");	


	        } 
	
	
	finally{        
	        
		
		
		//System.out.println(session.isConnected());

		    if (channel != null) {
		      session = channel.getSession();
		        channel.disconnect();
		        session.disconnect();
		       // System.out.println(channel.isConnected());
		    }
		
	      return adj_val_rds;
	}
	
	}


	public static boolean comparebtwjsonRDStorderrequests(HashMap<String, String> eachorderProposal,HashMap<String, String> fieldmapping, String CSV_Folder,String CSVFileName, ExtentTest logger, String Resultpath, String tcid, XWPFRun xwpfrun) throws IOException {
		boolean res=false;
		try{	
		
			File file = new File(CSV_Folder+CSVFileName);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			
			while(line != null)
			{
				HashMap<String,String> datafromcsv=new HashMap<String,String>();
			    String[] lines = line.split(",");
			    int cnt = 0;
			    for(String templine:lines){
			    	templine=templine.replace("\"", "").replace("{", "").replace("}", "").trim();
			    	if(templine.contains(":")){
			    		String[] inlinearr=templine.split(":");
			    		if(inlinearr[0].contains("Date")){
			    			String val=null;
			    			val=templine.substring(templine.indexOf(":")+1).trim();
			    			datafromcsv.put(inlinearr[0].trim(),val);
			    		}else
			    			if( inlinearr.length>1)
			    				datafromcsv.put(inlinearr[inlinearr.length-2].trim(), inlinearr[inlinearr.length-1].trim());
			    	}else{
			    		if(cnt<2){
			    			header=headers[cnt].trim();
				    		datafromcsv.put(header, templine);
			    		}else if(cnt==lines.length-1){
			    			header=headers[cnt-2].trim();
				    		datafromcsv.put(header, templine);
			    		}else if(cnt==6){
			    			header=headers[cnt-2].trim();
				    		datafromcsv.put(header, templine);
			    		}
			    		
			    	}
			    	cnt++;
			    }
			    System.setProperty("requestid", datafromcsv.get("requestid"));
			    String rdsvalue=null, jsonvalue=null;
			    int cnt1=0;
			    if(!(fieldmapping==null)){
			    	Iterator it = fieldmapping.entrySet().iterator();
				    
				    
				    while (it.hasNext()) {
				        Map.Entry pair = (Map.Entry)it.next();
				        System.out.println(pair.getKey() + " = " + pair.getValue());
				        rdsvalue=datafromcsv.get(pair.getKey());
				        jsonvalue=eachorderProposal.get(pair.getValue());
				        if(rdsvalue.trim().contains(jsonvalue.trim())){
				        	res=true;
				        	logger.log(LogStatus.PASS,"Validaion of "+pair.getKey()+":successful in torderrequests. Displayed value: "+jsonvalue.trim());
				        	utilityFileWriteOP.writeToLog(tcid, pair.getKey().toString(), "Displayed value: "+jsonvalue.trim(),Resultpath,xwpfrun);
				        	utilityFileWriteOP.writeToLog(tcid, pair.getKey().toString(), "Displayed value: "+jsonvalue.trim(),Resultpath);
				        }else{
				        	res=false;
				        	logger.log(LogStatus.FAIL,"Validation of "+pair.getKey()+":Unsuccessful in torderrequests");
				        	utilityFileWriteOP.writeToLog(tcid, pair.getKey().toString(), "Displayed value: "+jsonvalue.trim(),Resultpath,xwpfrun);
				        	utilityFileWriteOP.writeToLog(tcid, pair.getKey().toString(), "Displayed value: "+jsonvalue.trim(),Resultpath);
				        }
				        
				        	
				        it.remove(); // avoids a ConcurrentModificationException
				    }
			    }
			    
			   
		        	rdsvalue=datafromcsv.get("currentreqstatus");
		        	
		        	if(rdsvalue.trim().contains("requestRaised") && cnt1<1){
		        		 logger.log(LogStatus.PASS, "currentreqstatus in torderrequests table is displayed as "+rdsvalue);
		        		 utilityFileWriteOP.writeToLog(tcid, "currentreqstatus in torderrequests", rdsvalue,Resultpath,xwpfrun);
		        		 utilityFileWriteOP.writeToLog(tcid, "currentreqstatus in torderrequests", rdsvalue,Resultpath);
		        		 res=true;
		        	}else{
		        		logger.log(LogStatus.FAIL, "currentreqstatus in torderrequests table is displayed as "+rdsvalue);
		        		utilityFileWriteOP.writeToLog(tcid, "currentreqstatus in torderrequests", rdsvalue,Resultpath,xwpfrun);
		        		utilityFileWriteOP.writeToLog(tcid, "currentreqstatus in torderrequests", rdsvalue,Resultpath);
		        		res=false;
		        	}
		        System.setProperty("orderAlternateID", datafromcsv.get("orderalternateid"));
		        
		        cnt1++;
			    line = br.readLine();         
			}
			
			fr.close();
			return res;
		}catch(Exception e){
			e.printStackTrace();
			res=false;
			return res;
		}
	}


	public static boolean comparebtwjsonRDStorderrequestitems(HashMap<String, String> eachorderProposal,HashMap<String, String> fieldmapping, String CSV_Folder,String CSVFileName,String requestId, String packid, ExtentTest logger, String Resultpath, String tcid, XWPFRun xwpfrun) {
		boolean res=false;
		try{	
		
			File file = new File(CSV_Folder+CSVFileName);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			
			while(line != null)
			{
				HashMap<String,String> datafromcsv=new HashMap<String,String>();
			    String[] lines = line.split(",");
			    int cnt = 0;
			    for(String templine:lines){
			    	templine=templine.replace("\"", "").replace("{", "").replace("}", "").trim();
			    	if(templine.contains(":")){
			    		String[] inlinearr=templine.split(":");
			    		if(inlinearr[0].contains("Date")){
			    			String val=null;
			    			val=templine.substring(templine.indexOf(":")+1).trim();
			    			datafromcsv.put(inlinearr[0].trim(),val);
			    		}else
			    			datafromcsv.put(inlinearr[inlinearr.length-2].trim(), inlinearr[inlinearr.length-1].trim());
			    	}else{
			    		
			    			header=headers[cnt].trim();
				    		datafromcsv.put(header, templine);
			    		
			    		
			    	}
			    	cnt++;
			    }
			    Iterator it = fieldmapping.entrySet().iterator();
			    String rdsvalue=null, jsonvalue=null;
			    boolean resfail=true;
			    while (it.hasNext()) {
			        Map.Entry pair = (Map.Entry)it.next();
			        System.out.println(pair.getKey() + " = " + pair.getValue());
			        rdsvalue=datafromcsv.get(pair.getKey());
			        jsonvalue=eachorderProposal.get(pair.getValue());
			        resfail=true;
			        if(datafromcsv.get("lineid").contains(eachorderProposal.get(pair.getValue()))){
			        	if(jsonvalue.trim().contains(rdsvalue.trim())){
				        	res=true;
				        	logger.log(LogStatus.PASS,"Validaion of "+pair.getKey()+":successful in torderrequestitems. Value displayed as "+jsonvalue);
				        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Successful in torderrequestitems. Value displayed as "+jsonvalue,Resultpath,xwpfrun);
				        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Successful in torderrequestitems. Value displayed as "+jsonvalue,Resultpath);
				        }else{
				        	resfail=false;
				        	logger.log(LogStatus.FAIL,"Validaion of "+pair.getKey()+":Unsuccessful in torderrequestitems. Expected value: "+jsonvalue+". Actual Value"+rdsvalue);
				        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Not Successful in torderrequestitems. Value displayed as "+jsonvalue,Resultpath,xwpfrun);
				        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Not Successful in torderrequestitems. Value displayed as "+jsonvalue,Resultpath);
				        }
			        }else if(datafromcsv.get("packid").contains(eachorderProposal.get(pair.getValue())) || res==true){
			        	if(jsonvalue.trim().contains(rdsvalue.trim())){
				        	res=true;
				        	logger.log(LogStatus.PASS,"Validaion of "+pair.getKey()+":successful in torderrequestitems. Value displayed as "+jsonvalue);
				        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Successful in torderrequestitems. Value displayed as "+jsonvalue,Resultpath,xwpfrun);
				        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Successful in torderrequestitems. Value displayed as "+jsonvalue,Resultpath);
				        }else{
				        	resfail=false;
				        	logger.log(LogStatus.FAIL,"Validaion of "+pair.getKey()+":Unsuccessful in torderrequestitems. Expected value: "+jsonvalue+". Actual Value"+rdsvalue);
				        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Not Successful in torderrequestitems. Value displayed as "+jsonvalue,Resultpath,xwpfrun);
				        	utilityFileWriteOP.writeToLog(tcid, "Validaion of "+pair.getKey()+": ", "Not Successful in torderrequestitems. Value displayed as "+jsonvalue,Resultpath);
				        }
			        	
			        }
			        	
			        it.remove(); // avoids a ConcurrentModificationException
			    }
			    
			    if(resfail==true && res==true){
			    	logger.log(LogStatus.PASS, "Validation of data between json file and torderrequestitems table are successful for requestId: "+requestId+" and packid: "+packid);
				}else{
					logger.log(LogStatus.FAIL, "Validation of data between json file and torderrequestitems table are not successful for requestId: "+requestId+" and packid: "+packid);
					res=false;
				
			    }
			    line = br.readLine();         
			}
			
			fr.close();
			return res;
		}catch(Exception e){
			e.printStackTrace();
			/*res=false;
			return res;*/
		}
		return res;
	}


	public static String retrieveeventIdfromtOrderRequests(String CSV_Folder,String CSVFileName) {
		String res=null;
		try{	
			
			File file = new File(CSV_Folder+CSVFileName);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			
			while(line != null)
			{
				HashMap<String,String> datafromcsv=new HashMap<String,String>();
			    String[] lines = line.split(",");
			    int cnt = 0;
			    for(String templine:lines){
			    	templine=templine.replace("\"", "").replace("{", "").replace("}", "").trim();
			    	if(templine.contains(":")){
			    		String[] inlinearr=templine.split(":");
			    		if(inlinearr[0].contains("Date")){
			    			String val=null;
			    			val=templine.substring(templine.indexOf(":")+1).trim();
			    			datafromcsv.put(inlinearr[0].trim(),val);
			    		}else
			    			datafromcsv.put(inlinearr[inlinearr.length-2].trim(), inlinearr[inlinearr.length-1].trim());
			    	}else{
			    		if(cnt==0){
			    			header=headers[cnt].trim();
				    		datafromcsv.put(header, templine);
			    		}else if(cnt==lines.length-1){
			    			header=headers[cnt-2].trim();
				    		datafromcsv.put(header, templine);
			    		}
			    		
			    	}
			    	cnt++;
			    }
			    res=datafromcsv.get("eventID").trim();
			    line = br.readLine();         
			}
			
			fr.close();
			return res;
		}catch(Exception e){
			e.printStackTrace();
			res=null;
			return res;
		}
	}


	public static HashMap<String, String> ExtractfromRDS(String CSV_Folder,String CSVFileName,
			ArrayList<String> extractedvalue, ExtentTest logger) throws IOException {
		HashMap<String, String> res=new HashMap<String, String>();
		BufferedReader br = null;
		try{	
			
			File file = new File(CSV_Folder+CSVFileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			while(line != null)
			{
				HashMap<String,String> datafromcsv=new HashMap<String,String>();
			    String[] lines = line.split(",");
			    int cnt = 0;
			    for(String templine:lines){
			    	templine=templine.replace("\"", "").replace("{", "").replace("}", "").trim();
			    	if(templine.contains(":")){
			    		String[] inlinearr=templine.split(":");
			    		if(inlinearr[0].contains("Date")){
			    			String val=null;
			    			val=templine.substring(templine.indexOf(":")+1).trim();
			    			datafromcsv.put(inlinearr[0].trim(),val);
			    		}else
			    			datafromcsv.put(inlinearr[inlinearr.length-2].trim(), inlinearr[inlinearr.length-1].trim());
			    	}else{
			    		if(cnt==0){
			    			header=headers[cnt].trim();
				    		datafromcsv.put(header, templine);
			    		}else if(cnt==lines.length-1){
			    			header=headers[cnt-2].trim();
				    		datafromcsv.put(header, templine);
			    		}
			    		
			    	}
			    	cnt++;
			    }
			    for(String value:extractedvalue){
			    	res.put(value, datafromcsv.get(value).trim());
			    }
			    
			    line = br.readLine();         
			}
		}catch(Exception e){
			e.printStackTrace();
			res=null;
			return res;
		}finally{
			br.close();
		}
		return res;
	}


	public static boolean ValidateRDS(String CSV_Folder,String CSVFileName,
			HashMap<String, String> expectedvalues,String tablename,String itemids, ExtentTest logger) throws IOException {
		boolean res=false;
		BufferedReader br = null;
		try{	
			
			File file = new File(CSV_Folder+CSVFileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			if(line==null){
				logger.log(LogStatus.FAIL, "Validation in "+tablename+" table is not successful. The field is displayed as blank");
				res=false;
				return res;	
				
			}
			while(line != null)
			{
				try{
					
					String[] lineVal = line.split(",");
					String http_response_code=lineVal[0];
					
					String total_items_quantities=lineVal[1];
					String itemdetails=line.replace(http_response_code+",", "").replace(total_items_quantities+",", "");
					System.out.println(itemdetails);
					String[] itemidarr=itemids.split(",");
					System.out.println("1http_response_code: "+http_response_code);
					if(http_response_code.trim().contains("200")){
						res=true;
						System.out.println("true - http_response_code: "+http_response_code);
					}else{
						System.out.println("false - http_response_code: "+http_response_code);
						throw new MyException("Test Stopped because error code is not matching");
					}
					
					for(String itremid:itemidarr){
						System.out.println("itremid: "+itremid);
						res=itemdetails.contains(itremid);
					}
					
					
				}catch(Exception e){
					
				}
				
				line = br.readLine();
			}
			System.out.println("res: "+res);
		}catch(Exception e){
			logger.log(LogStatus.FAIL, "Error occured during validation of "+tablename);
			e.printStackTrace();
			res=false;
			return res;
		}finally{
			br.close();
		}
		return res;
		
	}
	
	public static boolean GetSourcepathFromRDSStckMvmnt(String RDSusername, String RDSpassword, String RDSHostName, int RDSPort,String Query ,String CSVFileName,  String ResultPath, String tcid, XWPFRun xwpfRun ) throws JSchException, BiffException, IOException{
		System.out.println("Inside RDS");
		 Channel channel=null;
		 Session session=null;

	     String QResult="";
	     Boolean res=false;
	        try{      
	                
	        	  JSch jsch = new JSch();
	              
	              java.util.Properties config = new java.util.Properties(); 
	              String command = "psql --host=rds-euw1-sitpg-freshbackbone-001-v00.ca107skwgezh.eu-west-1.rds.amazonaws.com api_stockmovementservice_v1_db_sit --user api_stockmovementservice_v1_user;";           
	              session = jsch.getSession(RDSusername,RDSHostName,RDSPort);
	              
	              config.put("StrictHostKeyChecking", "no");
		          session.setConfig(config);
	      
	              session.setPassword(RDSpassword);
	          
	          	  session.connect();
	              channel = session.openChannel("exec");
	              ((ChannelExec)channel).setCommand(command);
	              
	              channel.setInputStream(null);
	              ((ChannelExec)channel).setErrStream(System.err);
	              InputStream in = channel.getInputStream();
	            ((ChannelExec)channel).setPty(true);
	              
	              OutputStream out=channel.getOutputStream();
	              channel.connect();
	              
	              Thread.sleep(1000);
	              out.write(("bN2QpkW7Ja87Te9R"+"\n").getBytes());
			        Thread.sleep(1000);
			        out.flush();
			        Thread.sleep(2000);
	   
	      
	        out.write(("\\copy ("+Query+") to "+CSVFileName+" with CSV Header; \n").getBytes());
	        out.flush();    
	        Thread.sleep(5000);
	        BufferedReader br = new BufferedReader(new InputStreamReader(in));
	        String line;
	        byte[] tmp = new byte[4096];
	           
	        int i=0;  
	              
	        
	          while (in.available() > 0)
	           {
	          		i = in.read(tmp, 0, 4096);
               if (i < 0) 
               {
	                        break; 
	             }
	           }     
	         
	          	System.out.print(new String(tmp, 0, i));
	        	QResult=new String(tmp, 0, i);
	        	String QResultFinal=new String(tmp, 0, i).replace("copy ", ""); 
	        	if(QResult!=null)
	        	{ 
	        		res=true;
	        	System.out.println("QResult is "+QResult+" is this!!!");
	        	utilityFileWriteOP.writeToLog(tcid,"Query result stored in a CSV file named  ",CSVFileName ,ResultPath,xwpfRun,"");
			 
	        	utilityFileWriteOP.writeToLog(tcid, "Query Result is-", QResultFinal,ResultPath, xwpfRun, "") ;
	        	Thread.sleep(5000);
	        	}
	        	else
	        	{
                 res=false;
                 utilityFileWriteOP.writeToLog(tcid,"Query result is null  ", QResult,ResultPath,xwpfRun,"");
	        	}
				  channel.disconnect();	
				  session.disconnect();
				  Thread.sleep(5000);		  
				  out.close();		  
				  in.close();
				  
	        	}catch(Exception e)
			        {
						e.printStackTrace();
						res = false;
						utilityFileWriteOP.writeToLog(tcid, "Could not get Result from RDS due to ", e.toString(),ResultPath, xwpfRun,"") ;
						return res;
						
			        } 
			
			
			finally{

				    if (channel != null) 
				    {
				      session = channel.getSession();
				        channel.disconnect();
				        session.disconnect();
	   
				    }					
			      
			}
			        return res;
			}

	
	public static boolean ValidateRDSdirect(String CSV_Folder,String CSVFileName,
			HashMap<String, String> expectedvalues,String tablename, ExtentTest logger) throws IOException {
		boolean res=false;
		BufferedReader br = null;
		try{	
			
			File file = new File(CSV_Folder+CSVFileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			if(line==null){
				logger.log(LogStatus.FAIL, "Validation in "+tablename+" table is not successful. The field is displayed as blank");
				res=false;
				return res;	
				
			}
			while(line != null)
			{
				HashMap<String,String> datafromcsv=new HashMap<String,String>();
			    String[] lines = line.split(",");
			    int cnt = 0;
			    for(String templine:lines){
			    	templine=templine.replace("\"", "").replace("{", "").replace("}", "").trim();
			    	header=headers[cnt].trim();
		    		datafromcsv.put(header, templine);
			    	cnt++;
			    }
			    if(expectedvalues==null){
			    	throw new MyException("Test Stopped Because of Failure. Please check Execution log");
			    }
			    Iterator<Entry<String, String>> it = expectedvalues.entrySet().iterator();
			    while (it.hasNext()) {
			        Entry<String, String> pair = it.next();
//			        System.out.println(pair.getKey() + " = " + pair.getValue());
			        if(expectedvalues.get(pair.getKey()).contains(datafromcsv.get(pair.getKey()))){
			        	logger.log(LogStatus.PASS, "Validation in "+tablename+" table is successful for the field "+pair.getKey()+": "+datafromcsv.get(pair.getKey()));
			        	res= true;
			        }else{
			        	logger.log(LogStatus.FAIL, "Validation in "+tablename+" table is not successful for the field "+pair.getKey()+": "+datafromcsv.get(pair.getKey())+". Expected: "+expectedvalues.get(pair.getKey()));
			        	res=false;
			        	break;
			        }
			        it.remove(); // avoids a ConcurrentModificationException
			    }
			    
			    
			    line = br.readLine();         
			}
		}catch(Exception e){
			logger.log(LogStatus.FAIL, "Error occured during validation of "+tablename);
			e.printStackTrace();
			res=false;
			return res;
		}finally{
			br.close();
		}
		return res;
		
	}
  
	public static HashMap<String, String> ExtractfromRDSdirect(String CSV_Folder,String CSVFileName,
			ArrayList<String> extractedvalue, ExtentTest logger) throws IOException {
		HashMap<String, String> res=new HashMap<String, String>();
		BufferedReader br = null;
		try{	
			
			File file = new File(CSV_Folder+CSVFileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			while(line != null)
			{
				HashMap<String,String> datafromcsv=new HashMap<String,String>();
			    String[] lines = line.split(",");
			    int cnt = 0;
			    for(String templine:lines){
			    	templine=templine.replace("\"", "").replace("{", "").replace("}", "").trim();
			    	header=headers[cnt].trim();
		    		datafromcsv.put(header, templine);
			    	
		    		
		    		cnt++;
			    }
			    for(String value:extractedvalue){
			    	res.put(value, datafromcsv.get(value).trim());
			    }
			    
			    line = br.readLine();         
			}
		}catch(Exception e){
			e.printStackTrace();
			res=null;
			return res;
		}finally{
			br.close();
		}
		return res;
	}
	
	public static boolean ValidateRDSAutoshipped(
			String CSV_Folder,String CSVFileName,
			HashMap<String, String> expectedvalues,String tablename,String itemids, ExtentTest logger) throws IOException {
		boolean res=false;
		BufferedReader br = null;
		try{	
			
			File file = new File(CSV_Folder+CSVFileName);
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line, header;
			String[] headers=br.readLine().split(","); // consume first line and ignore
			line = br.readLine();
			if(line==null){
				logger.log(LogStatus.FAIL, "Validation in "+tablename+" table is not successful. The field is displayed as blank");
				res=false;
				return res;	
				
			}
			while(line != null)
			{
				try{
					
					String[] lineVal = line.split(",");
					String http_response_code=lineVal[0];
					
					
					
					
					System.out.println("1http_response_code: "+http_response_code);
					if(http_response_code.trim().contains("202")){
						res=true;
						System.out.println("true - http_response_code: "+http_response_code);
					}else{
						System.out.println("false - http_response_code: "+http_response_code);
						throw new MyException("Test Stopped because error code is not matching");
					}
					
					
					
					
				}catch(Exception e){
					
				}
				
				line = br.readLine();
			}
			System.out.println("res: "+res);
		}catch(Exception e){
			logger.log(LogStatus.FAIL, "Error occured during validation of "+tablename);
			e.printStackTrace();
			res=false;
			return res;
		}finally{
			br.close();
		}
		return res;
		
	}
}
