package BI_Utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FolderZipper {
	  public static void main(String[] a) throws Exception {
	    zipFolder("S:\\AutomationResultsSelenium\\Result_06-10-2017-20-43-40", "S:\\AutomationResultsSelenium\\Result_06-10-2017-20-43-40.zip");
	    
	    
	    
	  }

	  static public void zipFolder(String srcFolder, String destZipFile) throws Exception {
	    ZipOutputStream zip = null;
	    FileOutputStream fileWriter = null;

	    fileWriter = new FileOutputStream(destZipFile);
	    zip = new ZipOutputStream(fileWriter);

	    addFolderToZip("", srcFolder, zip);
	    zip.flush();
	    zip.close();
	  }

	  static private void addFileToZip(String path, String srcFile, ZipOutputStream zip)
	      throws Exception {

	    File folder = new File(srcFile);
	    if (folder.isDirectory()) {
	      addFolderToZip(path, srcFile, zip);
	    } else {
	      byte[] buf = new byte[1024];
	      int len;
	      FileInputStream in = new FileInputStream(srcFile);
	      zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
	      while ((len = in.read(buf)) > 0) {
	        zip.write(buf, 0, len);
	      }
	      in.close();
	    }
	  }

	  static private void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
	      throws Exception {
	    File folder = new File(srcFolder);

	    for (String fileName : folder.list()) {
	      if (path.equals("")) {
	        addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
	      } else {
	        addFileToZip(path + "/" + folder.getName(), srcFolder + "/" + fileName, zip);
	      }
	    }
	  }
	  public static boolean copyfile(String source, String dest){
		  boolean res=false;
		  try{
			  
			  Path sourceDirectory = Paths.get(source);
		      Path targetDirectory = Paths.get(dest);

		      //copy source to target using Files Class
		      Files.copy(sourceDirectory, targetDirectory);
		      res=true;
			  return res;
		  }catch(Exception e){
			  res=false;
			  return res;
		  }
		  
	  }
	 public static boolean purgeDirectory(File dir) {
		  boolean purgefiles=false;
		  try{
			  for (File file: dir.listFiles()) {
			        if (file.isDirectory())
			            purgeDirectory(file);
			        file.delete();
			        purgefiles=true;
			    }
			}catch(Exception e){
				purgefiles=true;
			}
		  return purgefiles;
		  }
	 public static String lastFileModified(String directorypath) {
		 File dir = new File(directorypath);
		 File [] files = dir.listFiles(new FilenameFilter() {
		     @Override
		     public boolean accept(File dir, String name) {
		         return name.endsWith(".doc");
		     }
		 });
		 long lastMod = Long.MIN_VALUE;
		 String choice = null;
		 for (File file : files) {
			 if (file.lastModified() > lastMod) {
				 choice = file.getName();
				 lastMod = file.lastModified();
			 }
	    }
		 return choice;
		}
		    
		    
	}