package BI_Extract_Testcases;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.server.handler.ImplicitlyWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import BI_Extract_Functions.Bi_Functions;
import BI_Extract_Functions.McColls_Amazon_RackSpace;
import BI_Utilities.*;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Validate_BIExtract_Day_3 {
	
	String URL;
	String DriverPath;
	String DriverName;
	String DriverType;
	String BrowserPath;
	String SheetName;
	String Final_Result="";
	String Downloadfilepath="";
	String TestDataPath;
	ExtentReports extent;
	ExtentTest logger;
	Integer stepnum=0;	
	String ResultPath=null;
	String downloadFilepath="";
	String WebprotalURL;//Portal_ProjectURL
	
	public WebDriver driver = null;
	public WebDriverWait wait = null;
	
	
	//********************Declaring Environment variable org.apache.commons.logging.Log**********************
		static {
			System.setProperty("org.apache.commons.logging.Log",
			                        "org.apache.commons.logging.impl.NoOpLog");
			}
		//*******************************************************************************************************
	
		
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
	}
	
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	@BeforeTest
	public void setUp() throws Exception {
		URL=ProjectConfigurations.LoadProperties("AWS_URL");		
		DriverPath=ProjectConfigurations.LoadProperties("AWS_DriverPath");
		DriverName=ProjectConfigurations.LoadProperties("AWS_DriverName");
		DriverType=ProjectConfigurations.LoadProperties("AWS_DriverType");
		BrowserPath=ProjectConfigurations.LoadProperties("AWS_BrowserPath");
		Downloadfilepath=ProjectConfigurations.LoadProperties("AWS_DownloadPath");
		TestDataPath=ProjectConfigurations.LoadProperties("AWS_TestDataPath");
		SheetName=ProjectConfigurations.LoadProperties("AWS_SheetName");
		WebprotalURL= ProjectConfigurations.LoadProperties("Portal_ProjectURL");
		
		System.setProperty(DriverName,DriverPath);
		HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
		chromePrefs.put("profile.default_content_settings.popups", 0);
		chromePrefs.put("download.prompt_for_download", "false");
		chromePrefs.put("download.", "false");
		chromePrefs.put("download.default_directory", System.getProperty("user.dir")+Downloadfilepath);
		System.out.println("Downloadpath"+System.getProperty("user.dir")+Downloadfilepath);
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("prefs", chromePrefs);
		options.setExperimentalOption("useAutomationExtension", false);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		cap.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(cap);
		wait=new WebDriverWait(driver, 20);			
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);		
		System.out.println("URL: "+URL);
		
		//*********************Setting up Result Path*****************************
		if(ResultPath==null){
			ResultPath=Reporting_Utilities.createResultPath("WholesaleAutomation"+getCurrentDate.getISTDateddMM()+"/BiExtract");
		}
		System.out.println("Result path is "+ResultPath);
		//************************************************************************
		
		
		//*********************************Initialising Extent Report Variables**********************************
		extent = new ExtentReports (System.getProperty("user.dir") +ResultPath+"WebServiceSeleniumReport.html", false);
		extent.loadConfig(new File(System.getProperty("user.dir")+"/extent-config.xml"));
		//*******************************************************************************************************		
	}

	
	@AfterTest
	public void tearDown() throws Exception {
		extent.endTest(logger);
		extent.flush();
		extent.close();	
		//driver.quit();
	}
	
	@Test
	public void test() throws InterruptedException,IOException {
		
		String TestKeyword = "BIExtract_FileValidate_Day_3";
		String TestCaseNo = null;
		String TestCaseName = null;
		String Keyword = null;
		String DriverSheetPath = TestDataPath;
		String driversheetname=SheetName;
		String consolidatedScreenshotpath="";
		String AwsUsername= null;
		String AwsPassword= null;
		String Jobname= null;
		String searchkey= null;
		String WebportalUsername = null;
		String WebportalPassword= null;
		String CustomerNames= null;
		String ProxyHost= null;
		String ProxyPort=null;
		String Sysusername= null;
		String Syspassword= null;
		String order_header= null;
		String ApiKey= null;
		String key= null;
		String Value= null;
		String targetheader= null;
		String targethost= null;
		String targetport = null;
		String ColumnName= null;
		String finalfolder=null;
		int daycount=0;
		
		
		int r = 0;
		try {
			
			int rows = 0;
			int occurances = 0;
			Workbook wrk1 = Workbook.getWorkbook(new File(TestDataPath));
			Sheet sheet1 = wrk1.getSheet(SheetName);
			rows = sheet1.getRows();
			Cell[] FirstRow = sheet1.getRow(0);
			Map<String, Integer> map = new HashMap<String, Integer>();
			for(int i=0; i < FirstRow.length; i++)
			{
				map.put(FirstRow[i].getContents().trim(), i);
			}
			
			for(r=1; r<rows; r++) {
				Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
				if(occurances>0){
			    	break;
				}
				
				if(Keyword.equalsIgnoreCase(TestKeyword)) {
					occurances=occurances+1;
					Keyword = sheet1.getCell(map.get("Keyword"), r).getContents().trim();
					TestCaseNo = sheet1.getCell(map.get("TestCaseNo"), r).getContents().trim();
					TestCaseName = sheet1.getCell(map.get("TestCaseName"), r).getContents().trim();
					AwsUsername= sheet1.getCell(map.get("AWS_Username"), r).getContents().trim();
					AwsPassword= sheet1.getCell(map.get("AWS_Password"), r).getContents().trim();
					Jobname= sheet1.getCell(map.get("ECS_Taskname"), r).getContents().trim();
					searchkey= sheet1.getCell(map.get("Bucket_name"), r).getContents().trim();
					WebportalUsername= sheet1.getCell(map.get("Webportal_Username"), r).getContents().trim();
					WebportalPassword= sheet1.getCell(map.get("Webportal_password"), r).getContents().trim();
					CustomerNames= sheet1.getCell(map.get("CustomerNames"), r).getContents().trim();
					ProxyHost= sheet1.getCell(map.get("ProxyHostName"), r).getContents().trim();
					ProxyPort= sheet1.getCell(map.get("ProxyPort"), r).getContents().trim();
					Sysusername= sheet1.getCell(map.get("SYSUserName"), r).getContents().trim();
					Syspassword= sheet1.getCell(map.get("SYSPassWord"), r).getContents().trim();
					order_header= sheet1.getCell(map.get("Get_Order_Header"), r).getContents().trim();
					key= sheet1.getCell(map.get("Key"), r).getContents().trim();
					ApiKey= sheet1.getCell(map.get("Api_key"), r).getContents().trim();
					Value= sheet1.getCell(map.get("Value"), r).getContents().trim();
					targetheader= sheet1.getCell(map.get("TargetHeader"), r).getContents().trim();
					targethost= sheet1.getCell(map.get("TargetHostName"), r).getContents().trim();
					targetport= sheet1.getCell(map.get("TargetPort"), r).getContents().trim();
					ColumnName= sheet1.getCell(map.get("Columnnames"), r).getContents().trim();
					finalfolder= "sla";
					daycount=3;
					
										
					logger = extent.startTest(TestCaseName);
					
					//--------------------Doc File---------------------
					XWPFDocument doc = new XWPFDocument();
			        XWPFParagraph p = doc.createParagraph();
			        XWPFRun xwpfRun = p.createRun();
					
					//****************************Create Path for Word Document and Screenshots****************************************
					File screenshotpath = new File(System.getProperty("user.dir")+ResultPath+"Screenshot/"+TestCaseNo+"/");
					screenshotpath.mkdirs();
					consolidatedScreenshotpath=System.getProperty("user.dir")+"/"+ResultPath+"Screenshot/"+TestCaseNo+"/"+TestCaseNo+"_";
					//****************************************************************************************************************** 
					
					//********************Declaring environment variables for stepnumber*******************					
					try{
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}catch(Exception e){
						System.setProperty("stepnumber", "1");
						stepnum=Integer.parseInt(System.getProperty("stepnumber"));	
					}
					
					//--------------------Functions to login and run ECS task---------------------------------

					driver.get(URL);		
					driver.manage().window().maximize();
					boolean loginAWS = McColls_Amazon_RackSpace.aws_login(driver, wait, TestCaseNo, AwsUsername, AwsPassword,ResultPath,xwpfRun, logger);
					if(loginAWS==true) {
						logger.log(LogStatus.PASS, "AWS login is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Login to Amazon Rackspace", "Error occured in login to Amazon Rackspace", "FAIL", ResultPath);
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
					stepnum++;
					
					boolean runECSTask=McColls_Amazon_RackSpace.Ecs_task_runner_job_trigger_new(driver,wait,TestCaseNo,ResultPath,ResultPath+"Screenshot/"+TestCaseNo+"/", xwpfRun, logger, Jobname);
					if(runECSTask==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Run ECS Task", "ECS task  triggered successfully", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Triggering of ECS task  is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Run ECS Task", "Error occured during running ECS task  Amazon Rackspace", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during running ECS task  in Amazon Rackspace");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
						
					boolean navigateToS3= McColls_Amazon_RackSpace.navigateToS3(driver, wait, TestCaseNo, ResultPath, xwpfRun, logger);
					if(navigateToS3==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to S3", "Navigation to S3 is successfull", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Navigate to S3 console is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Navigate to S3", "Navigation to S3 is not successfull", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during navigating to S3 console");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
						
					boolean DownloadS3file = McColls_Amazon_RackSpace.validateFileExistenceInPending(finalfolder, driver, wait, searchkey, TestCaseNo, ResultPath, xwpfRun, logger);
					if(DownloadS3file==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Download S3 file", "Download S3 file is successfull", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Download S3 file is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Download S3 file", "Download S3 file is not successfull", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during Download S3 file");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
						
					Thread.sleep(10000);
					driver.navigate().to(WebprotalURL);
					
					driver.get(WebprotalURL);
					driver.manage().window().maximize();
					
					boolean LoginToWebportal = BI_Extract_Functions.Bi_Functions.webportal_login(driver, wait, TestCaseNo, WebportalUsername, WebportalPassword, ResultPath, xwpfRun, logger);
					if(LoginToWebportal==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Login to Webportal", "Login to Webportal is successfull", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Login to Webportal is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Login to Webportal", "Login to Webportal is not successfull", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during Login to Webportal");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
						
					boolean ValidateOrderCount = BI_Extract_Functions.Bi_Functions.MatchOrderCountWithWebportal(daycount ,driver, wait, TestCaseNo, CustomerNames, ResultPath, xwpfRun, logger, System.getProperty("user.dir")+Downloadfilepath);
					if(ValidateOrderCount==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Validation of customer order count", "Validation of customer order count is successfull", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Validation of customer order count is successful");
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Validation of customer order count", "Validation of customer order count is not successfull", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during Validation of customer order count");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
					
					boolean LogoutFromAws= BI_Extract_Functions.Bi_Functions.Webportal_Logout(driver, wait, TestCaseNo, WebportalUsername, ResultPath, xwpfRun, logger);
					if(LoginToWebportal==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout to Webportal", "Logout to Webportal is successfull", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Logout to Webportal is successful");
						driver.close();
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Logout to Webportal", "Logout to Webportal is not successfull", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during Logout to Webportal");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}	
						stepnum++;
					
					
					
					boolean ValidatedetailsfromCSV= Bi_Functions.getOrderDetailsFromCSV(ColumnName, targethost, Integer.parseInt(targetport), targetheader, TestCaseNo, ProxyHost, Integer.parseInt(ProxyPort), Sysusername, Syspassword, order_header, ApiKey, key, Value,driver, wait, TestCaseNo,System.getProperty("user.dir")+Downloadfilepath, CustomerNames, xwpfRun, logger,ResultPath);
					if(ValidatedetailsfromCSV==true) {
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Orderdetails validation", "Orderdetails validation is successfull", "PASS", ResultPath);
						logger.log(LogStatus.PASS, "Orderdetails validation is successful");
						
					}else{
						Reporting_Utilities.writeStepToHTMLLog(TestCaseNo, stepnum.toString(), "Orderdetails validation", "Orderdetails validation is not successfull", "FAIL", ResultPath);
						logger.log(LogStatus.FAIL, "Error occured during Orderdetails validation");
						throw new MyException("Test Stopped Because of Failure. Please check Execution log");
					}
						stepnum++;
					
					if (loginAWS && runECSTask && navigateToS3 && DownloadS3file && LoginToWebportal && ValidateOrderCount && LogoutFromAws && ValidatedetailsfromCSV && LogoutFromAws){
						Final_Result="PASS";	 						 
						utilityFileWriteOP.writeToLog(TestCaseNo, "ClickView csv file validation done successfully", "PASS",ResultPath);
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);							  
						Assert.assertTrue(true);
						FileOutputStream out1 = new FileOutputStream(consolidatedScreenshotpath+Final_Result+".docx");
						doc.write(out1);
						out1.close();
						doc.close();
					}else {
						Final_Result="FAIL"; 
						excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
						utilityFileWriteOP.writeToLog(TestCaseNo, "ClickView csv file validation is not successfull", "FAIL",ResultPath);
						Assert.assertTrue(false);
						}
				}
			}
			
		}catch (Exception e) {

			e.printStackTrace();
			Final_Result="FAIL"; 
			excelCellValueWrite.writeValueToCell(Final_Result, r, 3, DriverSheetPath, driversheetname);
			Assert.assertTrue(false);
			}
			finally
			{  
			RowGenerator.collateHTMLLog(TestCaseNo, ResultPath);
			logger.log(LogStatus.INFO, "Evidance link for - "+TestCaseName+" <a href='file:///"+consolidatedScreenshotpath+Final_Result+".docx"+"'>EvidenceLink</a>");
			}
		
	}

}
