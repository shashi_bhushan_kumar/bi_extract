package BI_Extract_Functions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.opencsv.CSVIterator;
import com.opencsv.CSVParser;
import com.opencsv.CSVReader;







import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import BI_Utilities.TakeScreenShot;
import BI_Utilities.getCurrentDate;
import BI_Utilities.utilityFileWriteOP;

import com.fasterxml.jackson.databind.ObjectMapper;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

public class Bi_Functions {
	
	/*
	* Method Name: Validate order count
	* Functionality Description: Validate the count of order in clickview file for each customer
	* Created on: 08/01/2020
	* Created By: Pritam Debnath
	*/
	public static Map<String, Integer> ValidateOrderCount(String DownloadPath) throws IOException{
		String Filename= System.getProperty("S3Filename");
		String Fpath =DownloadPath+Filename;   
	    String line;
	    File file = new File(Fpath);
	    int count;
	    String Customername= null;
	    BufferedReader bufRdr = new BufferedReader(new FileReader(file));
	    Map<String, Integer> Customercount = new HashMap<String, Integer>();
	    Customercount.put("amazon", 0);
	    Customercount.put("harvest", 0);
	    Customercount.put("mccolls", 0);
	    Customercount.put("mcdly", 0);
	    Customercount.put("mpk", 0);
	    Customercount.put("rontec", 0);
	    Customercount.put("rontec-b", 0);
	    Customercount.put("sandpiper", 0);
	    String header = bufRdr.readLine();
	    while((line = bufRdr.readLine()) != null){
	        String[] cell= line.split(",");
	        if (cell[21].equals("1")){
	        	Customername= cell[0];
	        	count= Customercount.get(Customername);
	        	count++;
	        	Customercount.put(Customername, count);
	        }
	    }
	    return Customercount;
	}
		
	/*
	 * Method Name : webportal_login
	 * Method Description : Login to Wholesale Webportal
	 * Created By : Pritam Debnath
	 * Created Date : 08/01/2020
	 */
	public static boolean webportal_login(WebDriver driver, WebDriverWait wait, String tcid, String un, String pw,String Resultpath,XWPFRun xwpfrun, ExtentTest logger)
	{
		boolean res=false;
		try
		{	
			//Resultpath=System.getProperty("user.dir")+"/"+Resultpath;
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='username']")));
			String wholesale_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	    
			TakeScreenShot.saveScreenShot(wholesale_sc, driver);
			utilityFileWriteOP.writeToLog(tcid, "Login Page of wholesale webportal", "Displayed!!!",Resultpath,xwpfrun);
			logger.log(LogStatus.PASS, "Login Page of wholesale webportal is displayed");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc));			
			driver.findElement(By.xpath("//input[@id='username']")).sendKeys(un);			
			String wholesale_sc10=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	    
			TakeScreenShot.saveScreenShot(wholesale_sc10, driver);
			utilityFileWriteOP.writeToLog(tcid, "Username", "Entered!!!",Resultpath,xwpfrun);
			logger.log(LogStatus.PASS, "Username is Entered");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc10));			
			driver.findElement(By.xpath("//input[@id='password']")).sendKeys(pw);			
			String wholesale_sc12=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	    
			TakeScreenShot.saveScreenShot(wholesale_sc12, driver);
			utilityFileWriteOP.writeToLog(tcid, "Password", "Entered!!!",Resultpath,xwpfrun);
			logger.log(LogStatus.PASS, "Password is Entered");
			logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc12));			
			driver.findElement(By.xpath("//button[@id='submit']")).click();		
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")));
				       
						
			if(driver.findElement(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]")).isDisplayed())
			{
				res=true;
				String wholesale_sc13=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(wholesale_sc13, driver);
				logger.log(LogStatus.PASS, "User logged in successfully");
				logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc13));
				utilityFileWriteOP.writeToLog(tcid, "User logged in", "successfully!!!",Resultpath,xwpfrun);				
			}else{
				String wholesale_sc14=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(wholesale_sc14, driver);
				logger.log(LogStatus.FAIL, "User not logged in successfully");
				logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc14));
				utilityFileWriteOP.writeToLog(tcid, "User not logged in", "successfully!!!",Resultpath,xwpfrun);				
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			String awsDynamoDB_sc=Resultpath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	   
			TakeScreenShot.saveScreenShot(awsDynamoDB_sc, driver);
			res=false;
			logger.log(LogStatus.FAIL, e);
			logger.log(LogStatus.FAIL, logger.addScreenCapture(awsDynamoDB_sc));
			utilityFileWriteOP.writeToLog(tcid, "A problem occurred while navigating to order enquiery page", "Due to "+e,Resultpath);
		}
		finally
		{
			return res;
		}	
	}
	
	/*
	 * Method Name : MatchOrderCountWithWebportal
	 * Method Description : Get the order count from webportal and match it with clickview file
	 * Created By : Pritam Debnath	
	 * Created Date : 08/01/2020
	 */	
	public static boolean MatchOrderCountWithWebportal( int daycount, WebDriver driver, WebDriverWait wait, String tcid,String CustomerNames, String ResultPath,XWPFRun xwpfRun, ExtentTest logger, String DownloadPath){
    	
		boolean res=false;
		boolean result=true;
    	try{   		
    		//ResultPath=System.getProperty("user.dir")+"/"+ResultPath;
    		String[] individualCustomer = CustomerNames.split(",");
    		Map<String, Integer> OrderdetailsfromWebportal=  new HashMap<String, Integer>();
    		
    		for (int i=0; i<individualCustomer.length;i++){
    			driver.findElement(By.xpath("//img[@alt='"+individualCustomer[i]+"']")).click();   			
    			logger.log(LogStatus.PASS, "Clicked on '"+individualCustomer[i]+"'");
    			String wholesale_sc1=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
    			TakeScreenShot.saveScreenShot(wholesale_sc1, driver);
    			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc1));
    			utilityFileWriteOP.writeToLog(tcid, "Required Customer", "Clicked!!!",ResultPath,xwpfRun);
    			
    			driver.findElement(By.xpath("//h5[contains(text(),'Order Enquiry')]")).click();    			    			
    			logger.log(LogStatus.PASS, "User  navigated to Sales order page successfully for '"+individualCustomer[i]+"'");
    			String wholesale_sc2=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
    			TakeScreenShot.saveScreenShot(wholesale_sc2, driver);
    			utilityFileWriteOP.writeToLog(tcid, "Sales order page", "displayed!!!",ResultPath,xwpfRun);
    			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc2));
    			
    			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h4[contains(text(),'Sales Orders')]")));  			
    			wait.until(ExpectedConditions.elementToBeClickable(By.id("advSearchIcon"))).isDisplayed();
                driver.findElement(By.id("advSearchIcon")).click();                              
    			logger.log(LogStatus.PASS, "Advanced button , Advance Search Criteria for Sales Orders displayed successfully for  ");
    			String wholesale_sc3=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
    			TakeScreenShot.saveScreenShot(wholesale_sc3, driver);
    			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc3));
    			utilityFileWriteOP.writeToLog(tcid, "Advance search page is", "displayed!!!",ResultPath,xwpfRun);
    			
                JavascriptExecutor js = (JavascriptExecutor) driver;  
                js.executeScript("window.scrollBy(0,1000)");                
                driver.findElement(By.xpath("//*[@id='adv-search-desk']")).click();
                Thread.sleep(500);
                
                if(driver.findElement(By.xpath("//strong[contains(text(),'Select atleast 1 field from Order Date')]")).isDisplayed()){
    				res=true;
    			}else{
    				res=false;
    			}
                
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//i[@id='deliverydatefrom'])[2]")));
    			driver.findElement(By.xpath("(//i[@id='deliverydatefrom'])[2]")).click();  			
    			Thread.sleep(200);
    			
    			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    	        Calendar cal = Calendar.getInstance();
    	        cal.add(Calendar.DATE, -daycount);
    	        Date todate1 = cal.getTime();    
    	        String date = dateFormat.format(todate1);
    			String[] dmy= date.split("/");
    			String day=dmy[0];
    			String[] monthList={"January","February","March","April","May","June","July","August","September","October","November","December"};    			
    			int monthIndex= Integer.parseInt(dmy[1]);
    			String month=monthList[monthIndex-1];
    			String year=dmy[2];
    			long y= Integer.parseInt(year);
    			Thread.sleep(2000);
    			    						   			
                driver.findElement(By.xpath("(//datepicker[@id='deliverydateinput']//table//th)[2]//strong")).click();
    			Thread.sleep(500);
    			
                int yr = Calendar.getInstance().get(Calendar.YEAR);                                
                for(int in=0;in<(yr-y);in++)
                	driver.findElement(By.xpath("//button[@class='btn btn-default btn-sm pull-left']")).click();
                
                Thread.sleep(500);
                driver.findElement(By.xpath("//span[contains(text(),'"+month+"')]")).click();               				
    			Thread.sleep(5000);
    			WebElement element5=driver.findElement(By.xpath("//datepicker[@id='deliverydateinput']//span[contains(text(),'"+ day +"')]"));                     
                JavascriptExecutor executor5 = (JavascriptExecutor)driver;
                executor5.executeScript("arguments[0].click()", element5);                     
     			logger.log(LogStatus.PASS, "Delivery date "+date+" is entered for "+individualCustomer[i]);
     			String wholesale_sc4=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";	       
     			TakeScreenShot.saveScreenShot(wholesale_sc4, driver);
     			utilityFileWriteOP.writeToLog(tcid, "Delivery date is", "entered!!!",ResultPath,xwpfRun);
     			logger.log(LogStatus.INFO, logger.addScreenCapture(wholesale_sc4));                                          
                Thread.sleep(5000);
                driver.findElement(By.xpath("//button[@id='adv-search-desk' and contains(text(),'SUBMIT')]")).click();
                try{
                	driver.findElement(By.xpath("(//p[contains(text(),'No data available')])[2]")).isDisplayed();
                	OrderdetailsfromWebportal.put(individualCustomer[i], 0);
                	result=true;
                }catch(Exception e){
                	result=false;
                	System.out.println("Result: "+result);
                }
                if(result==false){
                	wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='table-responsive table-block' and @id='exportable1']/table"))));
                    java.util.List<WebElement> rows = driver.findElements(By.xpath("//div[@class='table-responsive table-block' and @id='exportable1']/table/tbody/tr"));
                    int ordercount=rows.size();
                    OrderdetailsfromWebportal.put(individualCustomer[i], ordercount);
                    System.out.println("Ordercount for customer" + individualCustomer[i] + "is:" +ordercount);
                }
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='bradcrumb-text' and contains(text(),'Home')]")));
                driver.findElement(By.xpath("//span[@class='bradcrumb-text' and contains(text(),'Home')]")).click();
                wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//h3[contains(text(),'SELECT CUSTOMER')]"))));
    		}	
    		
    		Map<String, Integer> OrderCountFromCSV= new HashMap<String, Integer>();
    		OrderCountFromCSV= ValidateOrderCount(DownloadPath);
    		
    		for (int k=0; k<individualCustomer.length;k++){
    			int ordercountCSV= OrderCountFromCSV.get(individualCustomer[k]);
    			int ordercountfromPortal= OrderdetailsfromWebportal.get(individualCustomer[k]);
    			if(ordercountCSV==ordercountfromPortal){
    				logger.log(LogStatus.PASS, "Ordercount from webportal: "+ordercountfromPortal+" is matched with ordercount from csv: "+ordercountCSV+"for customer"+individualCustomer[k]);
    			}else{
    				logger.log(LogStatus.FAIL, "Ordercount from webportal: "+ordercountfromPortal+" not matched with ordercount from csv: "+ordercountCSV+"for customer"+individualCustomer[k]);
    			}
    			
    		}
            res=true;
    		return res;
    	}
    	catch(Exception e)
    	{
    		res=false;		
    		e.printStackTrace();
    	}
		return res;    	
    }
	
	/*
	 * Method Name : Webportal_Logout
	 * Method Description : Logout from Webportal
	 * Created By : Pritam Debnath	
	 * Created Date : 09/01/2020
	 */			
	 public static boolean Webportal_Logout(WebDriver driver, WebDriverWait wait, String tcid, String poratlUserName,String ResultPath,XWPFRun xwpfrun, ExtentTest logger) 
		{			
			boolean res =false;
			try
			{
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'"+poratlUserName+"')]")));
				Thread.sleep(3000);
				driver.findElement(By.xpath("//button[contains(text(),'"+poratlUserName+"')]")).click();							
		        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Logout')]")));
		        driver.findElement(By.xpath("//a[contains(text(),'Logout')]")).click();		         
		        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@id='username' and @placeholder='Email address']")));
		        String wholesale_sc9=ResultPath+"Screenshot/"+tcid+"/"+tcid+getCurrentDate.getTimeStamp()+".png";
				TakeScreenShot.saveScreenShot(wholesale_sc9, driver);									
				if (driver.findElement(By.xpath("//input[@id='username' and @placeholder='Email address']")).isDisplayed())
				{
					res=true;
					logger.log(LogStatus.PASS, "User logged OUT successfully");
					logger.log(LogStatus.PASS, logger.addScreenCapture(wholesale_sc9));
					utilityFileWriteOP.writeToLog(tcid, "User logged OUT", "successfully!!!",ResultPath,xwpfrun);
				}else{
					res=false;
					logger.log(LogStatus.FAIL, "User not logged OUT successfully");
					logger.log(LogStatus.FAIL, logger.addScreenCapture(wholesale_sc9));
					utilityFileWriteOP.writeToLog(tcid, "User not logged OUT", "successfully!!!",ResultPath,xwpfrun);
					}
					
			}catch(Exception e)
			{
				res=false;
				return res;
			}
			finally
			{
				return res;
			}
		}

	/*
	 * Method Name : GetOrderItemDetailsFromWebservice
	 * Method Description : To get the order item details from webservice to match with the csv
	 * Created By : Pritam Debnath	
	 * Created Date : 09/01/2020
	 */	
	 public static boolean GetOrderItemDetailsFromWebservice(String columnnames, String customername, HashMap<String, HashMap<String, String>> CustomerOrderdetails,String Orderid, String targethost, int targetport,String targetheader,String TestCaseNo,String ProxyHost, int ProxyPort, String Sysusername,String Syspassword, String order_header, String ApiKey, String key, String Value ,XWPFRun xwpfrun, ExtentTest logger, String ResultPath){
		 
		 //System.out.println("Orderid: "+Orderid);
		 String[] columnnamesArray= columnnames.split(",");
		 boolean res= true;
		 int MaxwaitingTime = 120000;				
		 int regularWaitingTime = 10000;				
		 CloseableHttpResponse response=null;			    
		 CredentialsProvider credsProvider = new BasicCredentialsProvider();				
		 credsProvider.setCredentials(
		    		new AuthScope(ProxyHost, ProxyPort),
		    		new UsernamePasswordCredentials(Sysusername, Syspassword));
		 CloseableHttpClient httpclient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
		 try{	
			 			 
			 HttpHost target = new HttpHost(targethost, targetport, targetheader);
		     HttpHost proxy = new HttpHost(ProxyHost,ProxyPort);		        
		     RequestConfig config = RequestConfig.custom()
		                .setProxy(proxy)
		                .build();		        
		     HttpGet httpget = new HttpGet(order_header+ customername +"/orders" +"/"+ Orderid + "/items/?" + ApiKey);		        
		     httpget.setConfig(config);		        
		     httpget.addHeader(key, Value);
		     response = httpclient.execute(target, httpget);
		     System.out.println("response is: "+String.valueOf(response.getStatusLine().getStatusCode()) );
		     String jsonresponse = EntityUtils.toString(response.getEntity());
		     JsonElement jsonelement = new JsonParser().parse(jsonresponse);
		     JsonObject jsonobject = jsonelement.getAsJsonObject();		     
		     JsonArray jarray = jsonobject.getAsJsonArray("items");		     		     		              
		     ObjectMapper mapper = new ObjectMapper();
		     HashMap<String, String> map = null;
		     HashMap<String, HashMap<String, String>> OrderlinedetailsinWebservice= new HashMap<String, HashMap<String,String>>();
		     HashMap<String, String> OrderdetailsfromCSv= new HashMap<String, String>();
		     HashMap<String, String> Orderdetailswebservice= new HashMap<String, String>();
		     
		     for(int i=0; i<jarray.size(); i++){
		    	 String json = jarray.get(i).toString();
		    	 map = mapper.readValue(json, HashMap.class);
		    	 OrderlinedetailsinWebservice.put(String.valueOf(map.get("itemLineId")), map);		    	 		    	 
		     }
		     
/*		     for (int l=1; l<=OrderlinedetailsinWebservice.size();l++){
		    	 System.out.println("Webservice map size: "+OrderlinedetailsinWebservice.size());
		    	 Orderdetailswebservice= OrderlinedetailsinWebservice.get(Integer.toString(l));
		    	 System.out.println("Order details in WEBSERVICE: "+Orderdetailswebservice);
		     }
		     
		     for (int k=1; k<=CustomerOrderdetails.size();k++){
		    	 System.out.println("CSV map size: "+CustomerOrderdetails.size());
		    	 OrderdetailsfromCSv= CustomerOrderdetails.get(Integer.toString(k));
		    	 System.out.println("Order details in CSV: "+OrderdetailsfromCSv);
		     }
		     */
		     if (OrderlinedetailsinWebservice.size()==CustomerOrderdetails.size()){		    	 
		    	 logger.log(LogStatus.PASS, "Linecount matched for orderid: "+ Orderid);
		    	 //utilityFileWriteOP.writeToLog(TestCaseNo, "Order linecount match", "Failed!!!",ResultPath,xwpfrun);
		    	 for (int j=1; j<=OrderlinedetailsinWebservice.size(); j++){
		    		 Orderdetailswebservice= OrderlinedetailsinWebservice.get(Integer.toString(j));
		    		 OrderdetailsfromCSv= CustomerOrderdetails.get(Integer.toString(j));
		    		 for(int n=0; n<columnnamesArray.length; n++){
		    			 String columnvaluefromCSV= OrderdetailsfromCSv.get(columnnamesArray[n]);
		    			 String columnvaluefromWebservice= String.valueOf(Orderdetailswebservice.get(columnnamesArray[n]));
		    			 if (columnvaluefromCSV.equals(columnvaluefromWebservice)){		    				 
		    				 logger.log(LogStatus.PASS, "Value matched for column: "+columnnamesArray[n]+ " for line id: "+j+ " for orderid: "+Orderid);
		    		    	 //utilityFileWriteOP.writeToLog(TestCaseNo, "Value matching for column: "+columnnamesArray[n]+ " for line id: "+j, " Successfull!!!",ResultPath,xwpfrun);
		    			 }
		    			 else{
		    				 res=false;
		    				 logger.log(LogStatus.FAIL, "Value not matched for column: "+columnnamesArray[n]+ " for line id: "+j+ " for orderid: "+Orderid);
		    		    	 //utilityFileWriteOP.writeToLog(TestCaseNo, "Value matching for column: "+columnnamesArray[n]+ " for line id: "+j, " Failed!!!",ResultPath,xwpfrun);
		    			 }	    				 		    		
		    		 }
		    	 }
		     }
		     else{
		    	 res=false;
		    	 logger.log(LogStatus.FAIL, "Linecount matched for orderid: "+ Orderid);
		    	 //utilityFileWriteOP.writeToLog(TestCaseNo, "Order linecount match", "Failed!!!",ResultPath,xwpfrun);		    	 
		     }

		 }catch(Exception e){
			 res=false;
			 e.printStackTrace();
			 
		 }
		 
		 
		 return res;
	 }
	 
	 /*
	  * Method Name : getOrderDetailsFromCSV
	  * Method Description : To get the order item details from ClickView CSV
	  * Created By : Pritam Debnath	
	  * Created Date : 15/01/2020
	 */	
	 public static boolean getOrderDetailsFromCSV(String ColumnName,String targethost, int targetport,String targetheader,String TestCaseNo,String ProxyHost, int ProxyPort, String Sysusername,String Syspassword, String order_header, String ApiKey, String key, String Value,WebDriver driver, WebDriverWait wait, String tcid, String Downloadpath, String customernames,XWPFRun xwpfrun, ExtentTest logger, String ResultPath) throws IOException{
		 boolean res= true;		 
		 try{
			 
			 //ResultPath=System.getProperty("user.dir")+ResultPath;
			 String Filename= System.getProperty("S3Filename");
			 //String Filename= "wholesale_orders_20200109121829.csv";
			 //String Downloadpath1="S:/WholesaleNew/Reflexis_WS_P/ReflexisWorkforceScheduler_Maven/DownloadedFiles/";
			 String Fpath =Downloadpath+Filename;   
			 String line;
			 File file = new File(Fpath);
			 int count;
			 String Customername= null;
			 String flag=null;
			 int linecount;
			 BufferedReader bufRdr = null;
			 String[] CustomernameArray = customernames.split(",");
			 HashMap<String, String> orderlinedetails = null;
			 HashMap<String, HashMap<String, String>> orderdetails = null;
			 String Orderid = null;
			 for (int i=0; i<CustomernameArray.length; i++){
				 bufRdr= new BufferedReader(new FileReader(file));
				 orderdetails = new HashMap<String, HashMap<String, String>>();	
				 Orderid=null;
				 flag=null;
				 linecount=0;
				 while((line = bufRdr.readLine()) != null){
				        String[] cell= line.split(",");
				        Orderid = cell[1];
				        String lineid = cell[21];
				        if(linecount!=0 && !Orderid.equals(flag)) {
				        	break;
				        }				        
				        if(cell[0].equals(CustomernameArray[i])){
				        	String linedetails = line;				        	
				        	orderlinedetails = readCatalogue(Fpath, Orderid, lineid);
				        	orderdetails.put(lineid, orderlinedetails);					        	
				        	flag=Orderid;
				        	linecount++;
				        }				        
				 }
				 //System.out.println("Orderid: "+flag);
				 if (flag==null || flag.equals("")){
					 logger.log(LogStatus.PASS, "Order not found in generated CSV for customer: "+CustomernameArray[i]);
			    	 //utilityFileWriteOP.writeToLog(TestCaseNo, "Order not found in generated CSV for customer: "+CustomernameArray[i], "Successfull!!!",ResultPath,xwpfrun);
					 continue;
				 }
				 //--------------------------------call postman------------------------------------
				 boolean MatchOrderDetails = Bi_Functions.GetOrderItemDetailsFromWebservice(ColumnName, CustomernameArray[i],orderdetails,flag, targethost, targetport, targetheader, TestCaseNo, ProxyHost, ProxyPort, Sysusername, Syspassword, order_header, ApiKey, key, Value, xwpfrun, logger, ResultPath);
				 if (MatchOrderDetails){
					 logger.log(LogStatus.PASS, "Orderdetails matched for orderid: "+ flag + " For customer: "+CustomernameArray[i]);
			    	 //utilityFileWriteOP.writeToLog(TestCaseNo, "Orderdetails matched for orderid: "+ flag + " For customer: "+CustomernameArray[i], "Successfull!!!",ResultPath,xwpfrun);
				 }
				 else{
					 res=false;
					 logger.log(LogStatus.FAIL, "Orderdetails not matched for orderid: "+ flag + " For customer: "+CustomernameArray[i]);
			    	 //utilityFileWriteOP.writeToLog(TestCaseNo, "Orderdetails not matched for orderid: "+ flag + " For customer: "+CustomernameArray[i], "Failed!!!",ResultPath,xwpfrun);
				 }
			 }	 
		 }catch(Exception e){
			 e.printStackTrace();
			 res=false;
		 }
		 return res;
	 }
	  
	 /*
	  * Method Name : readCatalogue
	  * Method Description : To get orderline details in map
	  * Created By : Pritam Debnath	
	  * Created Date : 20/01/2020
	 */
	 public static HashMap<String, String> readCatalogue(String filePath, String item1, String item2) throws IOException
		{
		String[] columnnames=null;
		HashMap<String, String> mapfordoubleitem = new HashMap<String, String>();
		File file = new File(filePath);
		FileReader csvFile = new FileReader(file);
		CSVReader csvreader = new CSVReader(csvFile);
		boolean eof= false;
		int counter =0 ;
		int mincolumnindex1= 0;
		int mincolumnindex2= 0;
		while(!eof)
		{
			//System.out.println("counter: "+counter);
			if(counter == 0)
			{
				columnnames = csvreader.readNext();
				for(String l : columnnames)
				{
					//System.out.println(l);
					if(l.equals("orderId"))
					{
						break;
					}
					mincolumnindex1++;
				}
				for(String k : columnnames)
				{
					//System.out.println(k);
					if(k.equals("itemLineId"))
					{
						break;
					}
					mincolumnindex2++;
				}
			}
			else
			{
				String[] line= csvreader.readNext();
				if(line[mincolumnindex1].equals(item1) && line[mincolumnindex2].equals(item2))
				{
					for(int count=0;count<line.length;count++)
					{
						mapfordoubleitem.put(columnnames[count], line[count]);
						//System.out.println(columnnames[count] +" : "+line[count]);
					}
					eof=true;
				}				
			}
			counter++;
		}
		csvreader.close();
		csvFile.close();		
		return mapfordoubleitem;
		}
	 
}
